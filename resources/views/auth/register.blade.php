<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register</title>
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/flag-icon.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/simple-line-icons.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/assets/css/core-ui.css">
</head>
<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card mx-4">
          <form class="card-body p-4" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
            <h1>Register</h1>
            <p class="text-muted">Create your account</p>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="icon-user"></i></span>
              </div>
              <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}">
            </div>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">@</span>
              </div>
              <input type="text" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
            </div>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="icon-lock"></i></span>
              </div>
              <input type="password" class="form-control" placeholder="Password" name="password">
            </div>
            <div class="input-group mb-4">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="icon-lock"></i></span>
              </div>
              <input type="password" class="form-control" placeholder="Repeat password" name="password_confirmation">
            </div>
            <button type="submit" class="btn btn-block btn-success">Create Account</button>
          </form>
          <div class="card-footer p-4">
            <div class="row">
              <div class="col-12">
                Already have account? <a href="{{url('login')}}">Login</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/jquery.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/popper.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/bootstrap.min.js"></script>
  <script src="/assets/js/app.js"></script>

</body>
</html>
