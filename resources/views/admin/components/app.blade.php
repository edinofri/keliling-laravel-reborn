<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Panel
  @if(Request::segment(2))
      - {{ucwords(Request::segment(2))}}
  @endif
  </title>

  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/flag-icon.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/simple-line-icons.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/assets/css/core-ui.css">
  @yield('css')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden   pace-done pace-done">
  @include('admin.components.header')
  <div class="app-body">
    @include('admin.components.sidebar')
    <main class="main">
      <ol class="breadcrumb">
      <li class="breadcrumb-item">Home</li>
      <li class="breadcrumb-item"><a href="/administrator">Admin</a></li>
      @if(Request::segment(2))
      <li class="breadcrumb-item active">{{ucwords(Request::segment(2))}}</li>
      @endif
      </ol>
      @yield('content')

    </main>
  </div>


  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/jquery.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/popper.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/bootstrap.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/pace.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/Chart.min.js"></script>
  <script src="/assets/js/app.js"></script>
  @yield('js')

</body>
</html>
