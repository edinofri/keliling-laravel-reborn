<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/flag-icon.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/simple-line-icons.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/assets/css/core-ui.css">
</head>
<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="clearfix">
          <h1 class="float-left display-3 mr-4">404</h1>
          <h4 class="pt-3">Oops! You're lost.</h4>
          <p class="text-muted">The page you are looking for was not found.</p>
        </div>
      
      </div>
    </div>
  </div>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/jquery.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/popper.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/bootstrap.min.js"></script>
  <script src="/assets/js/app.js"></script>

</body>
</html>
