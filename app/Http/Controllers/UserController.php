<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Models\Roles;
use App\Models\UserRoles;
use Hash;

class UserController extends Controller
{
    public function index(Request $request){
          return User::all();
    }

    public function create(Request $request){
        $input = $request->input();
        try{
            $data = array(
                'name'=>$input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password'])
            );
        }catch(\ErrorException $e){
            $this->response_json->message = $e->getMessage();
            return $this->json();
        }

        if(User::where('email','=',$data['email'])->first()){
          $this->response_json->message = 'email already exist.';
          return $this->json();
        }
        $user = User::create($data);
        $role = Roles::where('name','user')->first();
        $userRoles = UserRoles::create(array(
            'user_id'=>$user->id,
            'role_id'=>$role->id
        ));
        $this->response_json->success = true;
        $this->response_json->message = 'success';
        $this->response_json->data->user = $user;
        $this->response_json->data->token = $this->generateToken($user);

        return $this->json();
    }

    public function login(Request $request){
      $input = $request->input();
      try{
          $data = array(
              'email' => $input['email'],
              'password' => $input['password']
          );
      }catch(\ErrorException $e){
          $this->response_json->message = $e->getMessage();
          return $this->json();
      }
      $user = User::where('email','=',$data['email'])->first();
      if($user == null){
        $this->response_json->message = 'email not register yet.';
        return $this->json();
      }
      if(Hash::check($data['password'],$user->password)){
        $this->response_json->success = true;
        $this->response_json->message = 'success';
        $this->response_json->data->user = $user;
        $this->response_json->data->token = $this->generateToken($user);
      }else{
        $this->response_json->message = 'wrong password';
      }

      return $this->json();

    }

}
