<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Response;
use App\Http\ResponseData;

use Firebase\JWT\JWT;
use App\User;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    protected $response_json;
    protected $JWT_KEY;
    protected $expired_token;
    protected $encode_token;
    protected $_sign;
    public function __construct()
    {
        $this->response_json = new ResponseData();
        $this->JWT_KEY = "MXV46XxCFRxNuB8LyAtmLDgixRnTAlMHjSACddwkyKem88eZtw9fzxz";
        $this->expired_token = "2"; // in hours
        $this->_sign = 'cisabgnakut';

    }

    public function json(){
        if($this->response_json->data == new \stdClass()){
            unset($this->response_json->data);
        }
        if($this->response_json->status == 404){
            $this->response_json->status = $this->response_json->success? 200:$this->response_json->status;
        }
        $this->response_json->status = $this->response_json->success ? 200 : 400;
        return response()->json($this->response_json,$this->response_json->status);
    }

    public function generateToken($user){
      $jwt_playload = [
        'user_id'=>$user->id,
        'role_id'=>$user->roles()[0]->role_id,
        'time'=>time(),
        'last_modified_account'=>$user->updated_at->__tostring()
      ];
      return $this->_sign.':'.JWT::encode($jwt_playload, $this->JWT_KEY);
    }

    // $this->auth($request);

    public function auth($request){
      if($this->isValid($request->header('Authorization'))){
        return User::find($this->encode_token->user_id)->first();
      }
      return null;
    }

    private function isValid($rawToken){
      try{
        $sign = explode(':',$rawToken)[0];
        $token = explode(':',$rawToken)[1];
        if($this->_sign != $sign){
          return false;
        }
        $this->encode_token = JWT::decode($token,$this->JWT_KEY,array('HS256'));
        return true;
      }catch(\Exception $e){
        return false;
      }
      return false;
    }
}
