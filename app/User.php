<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Roles;
use App\Models\UserRoles;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
      return UserRoles::where('user_id',$this->id)->get(['role_id']);
    }

    public function isAdministrator(){
      $administrator_roles = Roles::where('name','administrator')->first(['id']);
      foreach($this->roles() as $role){
        if($role->role_id == $administrator_roles->id){
          return true;
        }
      }
      return false;
    }
}
