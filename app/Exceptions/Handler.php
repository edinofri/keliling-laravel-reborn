<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\ResponseData;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];


    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    public function json(){
        if($this->response_json->data == new \stdClass()){
            unset($this->response_json->data);
        }
        if($this->response_json->status == 400){
            $this->response_json->status = $this->response_json->success? 200:400;
        }

        return response()->json($this->response_json,$this->response_json->status);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        $this->response_json = new ResponseData();

        if($request->is('api/*') && $e instanceof NotFoundHttpException){
            $this->response_json->status = 404;
            return self::json();
        }
        if($request->is('api/*') && $e instanceof MethodNotAllowedHttpException){
          $this->response_json->status = 405;
          $this->response_json->message = "Method not allowed.";
          return self::json();
        }

        if($e instanceof NotFoundHttpException){
          return response()->view('404');
        }

        // keep render view to make us easier to find source of problems
        return parent::render($request, $e);
    }
}
